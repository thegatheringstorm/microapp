const CellStatus = {
    Sure:'sure',
    Checked:'checked',
    Common:'common'
}

const DataStatus = {
    Checked:1,
    Common:2
}

const FuncStatus = {
    OpenWithDel:1,
    Open:2,
    Close:3
}

export {CellStatus,FuncStatus,DataStatus}