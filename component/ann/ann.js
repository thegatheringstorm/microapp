import { CellStatus, FuncStatus, DataStatus } from '../enum'

Component({
  properties: {
    // 这里定义了innerText属性，属性值可以在组件使用时指定
    innerText: {
      type: String,
      value: 'default value',
    }
  },

  data: {
    // 这里是一些组件内部数据
    contents: [
      { word: 'h', status: CellStatus.Common },
      { word: 'm', status: CellStatus.Common },
      { word: 'k', status: CellStatus.Common },
      { word: 'j', status: CellStatus.Sure, group_id: 'gid12345' },
      { word: 'o', status: CellStatus.Common },
      { word: 'k', status: CellStatus.Common },
      { word: 'p', status: CellStatus.Sure, group_id: 'gid2356' },
      { word: 'k', status: CellStatus.Sure, group_id: 'gid2356' },
      { word: 'k', status: CellStatus.Common },
      { word: 'k', status: CellStatus.Common },
      { word: 'k', status: CellStatus.Common },
      { word: 'k', status: CellStatus.Common },
      { word: 'k', status: CellStatus.Common },
      { word: 'k', status: CellStatus.Common },
      { word: 'k', status: CellStatus.Common },
      { word: 'k', status: CellStatus.Common },
      { word: 'k', status: CellStatus.Common },
      { word: 'k', status: CellStatus.Common },
      { word: 'k', status: CellStatus.Common },
      { word: 'k', status: CellStatus.Common },
      { word: 'k', status: CellStatus.Common },
      { word: 'k', status: CellStatus.Common },
      { word: 'k', status: CellStatus.Common },
      { word: 'k', status: CellStatus.Common },
      { word: 'k', status: CellStatus.Common },
      { word: 'k', status: CellStatus.Common },
      { word: 'e', status: CellStatus.Common }
    ],
    func_status: FuncStatus.Close,
    group: {'gid12345':'C1','gid2356':'C2'}
  },
  methods: {
    eraser_checked_status: function () {
      this.setData({
        contents: this.data.contents.map(
          x => {
            if (x.status == CellStatus.Checked) return { ...x, status: CellStatus.Common }
            else return x
          }
        )
      })
      this.eraser_key_position()
    },
    get_data_status: function () {
      if (this.data.contents.map(x => x.status).includes(CellStatus.Checked)) return DataStatus.Checked
      else return DataStatus.Common
    },
    set_func_status: function (status) {
      this.setData({ func_status: status })
    },
    set_checked_status: function (index) {
      const { contents } = this.data
      contents[index].status = CellStatus.Checked
      this.setData({ contents })
    },
    set_current_index: function (index) {
      this.setData({ current_index: index })
    },
    is_key_position: function (index) {
      if (this.data.key_position) {
        const result = (index == this.data.key_position.upper) || (index == this.data.key_position.lower)
        return result
      } else {
        return true
      }
    },
    set_key_position_lower: function (index) {
      const key_position = this.data.key_position
      this.setData({ key_position: { ...key_position, lower: index } })
    },
    set_key_position_upper: function (index) {
      const key_position = this.data.key_position
      this.setData({ key_position: { ...key_position, upper: index } })
    },
    set_key_position: function (index) {
      if (this.data.key_position) {
        const { lower, upper } = this.data.key_position
        if (index == this.data.key_position.lower) this.set_key_position_lower(lower - 1)
        else if (index == this.data.key_position.upper) this.set_key_position_upper(upper + 1)
        else console.log('set_key_position wrong!!!!')
      } else {
        this.setData({ key_position: { upper: index + 1, lower: index - 1 } })
      }
    },
    eraser_key_position: function () {
      this.setData({ key_position: null })
    },
    eraser_sure_status: function () {
      const index = this.data.current_index
      const group_id = this.data.contents[index].group_id
      this.setData({
        contents: this.data.contents.map(
          x => {
            if (x.group_id == group_id) {
              delete x.group_id
              x.status = CellStatus.Common
              return x
            }
            else return x
          }
        )
      })
      const {group} = this.data
      delete group[group_id]
      console.log(this.data.group)
    },
    del: function (event) {
      this.eraser_sure_status()
      console.log(event)
    },
    taphandler: function (event) {
      const { index } = event.target.dataset
      this.set_current_index(index)
      const content = this.data.contents[index]
      switch (content.status) {
        case CellStatus.Checked:
          break;
        case CellStatus.Sure:
          console.log('sure')
          this.eraser_checked_status()
          this.set_func_status(FuncStatus.OpenWithDel)
          break;
        case CellStatus.Common:
          console.log('common')
          if (this.get_data_status() == DataStatus.Checked && !this.is_key_position(index)) {
            this.eraser_checked_status()
            this.set_func_status(FuncStatus.Close)
          } else {
            this.set_func_status(FuncStatus.Open)
            this.set_checked_status(index)
            this.set_key_position(index)
          }
          break;
      }
      console.log(this.data.current_index)
    },
  }
})
